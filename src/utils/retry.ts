import delay from "delay";
import { Promisable } from "type-fest";

export async function retry<T>(
    job: (attempt: number) => Promisable<T>,
    count: number,
    interval: number,
): Promise<T> {
    let lastError = undefined;

    for (let attempt = 0; attempt < count; attempt++) {
        if (attempt > 0) await delay(interval);

        try {
            const result = await job(attempt);
            return result;
        }
        catch (error) {
            lastError = error;
        }
    }

    throw lastError;
}
