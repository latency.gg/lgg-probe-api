import assert from "assert";
import test from "tape-promise/tape.js";
import { withContext } from "../testing/index.js";

test("submit measurement v4", t => withContext(async context => {
    await initializeMocks();

    const client = context.createApplicationClient();

    const response = await client.submitMeasurementV4({
        parameters: {
            beaconIp: "127.0.255.250",
        },
        entity() {
            return {
                ip: "192.168.178.20",
                pub: "",
                rtt: 10,
                stddev: 0.5,
                type: "icmp",
                version: "v0.1.0",
                ident: "",
            };
        },
    });
    assert(response.status === 202);

    const responseEntity = await response.entity();

    t.ok(responseEntity);

    async function initializeMocks() {
        await context.services.pgPool.query(`
INSERT INTO public.beacon(
    id,
    ipv4,
    ipv6,
    location,
    provider,
    version
)
VALUES(
    '5457ef04-4702-4166-a996-b798474ee185',
    '127.0.255.250',
    '2001:db8::8a2e:370:7334',
    'example',
    'wikipedia',
    'v0.1.0'
);
`);
    }
}));
