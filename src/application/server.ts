import * as probeApi from "@latency.gg/lgg-probe-oas";
import { isHttpError } from "http-errors";
import * as operations from "../operations/index.js";
import { Context } from "./context.js";

export type Server = probeApi.Server;

export function createServer(
    context: Context,
) {
    const server = new probeApi.Server({
        baseUrl: context.config.endpoint,
        signal: context.signal,
    });

    server.registerMiddleware(
        async function (this, route, request, next) {
            const operation = route ?
                this.getOperationId(route.key, request.method) :
                undefined;

            const stopTimer = context.metrics.operationDuration.startTimer({ operation });
            try {
                const response = await next(request);
                stopTimer({ status: response.status });
                return response;
            }
            catch (error) {
                if (isHttpError(error)) stopTimer({ status: error.status });
                else stopTimer();

                throw error;
            }
        },
    );

    server.registerSubmitMeasurementV4Operation(
        operations.createMeasurementV4Operation(context),
    );

    server.registerSubmitMeasurementV6Operation(
        operations.createMeasurementV6Operation(context),
    );

    return server;
}
