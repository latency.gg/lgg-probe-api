import * as probeApi from "@latency.gg/lgg-probe-oas";
import assert from "assert";
import * as application from "../application/index.js";
import { withPgTransaction } from "../utils/index.js";

export function createMeasurementV4Operation(
    context: application.Context,
) {
    return operation;

    async function operation(
        incomingRequest: probeApi.IncomingSubmitMeasurementV4Request,
        authorization: {},
        acceptTypes: probeApi.AcceptSubmitMeasurementV4ContentTypes[],
        requestUrl: URL,
    ): Promise<probeApi.OutgoingSubmitMeasurementV4Response> {
        const { beaconIp } = incomingRequest.parameters;
        const measurementEntity = await incomingRequest.entity();

        const now = new Date();

        await withPgTransaction(
            context,
            "insert-measurement-v4",
            async pgClient => {
                const result = await pgClient.query(`
INSERT INTO public.measurement (
    source,
    timestamp_utc,
    rtt_ms, stddev, raw,
    beacon_version,
    beacon, measurement_type, client
)
SELECT
    $1,
    $2,
    $3, $4, $5,
    $6,
    beacon.id, measurement_type.id, 1
FROM public.measurement_type
CROSS JOIN public.beacon
WHERE measurement_type.name = $7
AND beacon.ipv4 = $8

RETURNING *
;
`,
                    [
                        measurementEntity.ip,
                        now.toISOString(),
                        measurementEntity.rtt, measurementEntity.stddev, measurementEntity.raw,
                        measurementEntity.version,
                        measurementEntity.type,
                        beaconIp,
                    ],
                );
                assert.equal(result.rowCount, 1);
            },
        );

        return {
            status: 202,
            parameters: {},
            entity() {
                return measurementEntity;
            },
        };

    }
}
